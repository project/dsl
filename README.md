INTRODUCTION
------------
This module provides the administrator interface 
to manage the site logo setting for each domain.


REQUIREMENTS
------------
Drupal 8.x


INSTALLATION
------------
1.  Place dsl module into your modules directory.
    This is normally the "modules" directory.

2.  Go to admin/modules. Enable modules.


CONFIGURATION
-------------

Domain Site Logo module have configuration setting page 
to set site logo setting against each domains.

Configuration page path:
/admin/config/domain/domain_site_settings

We override default logo varible.

You can you this "site_logo" varible in template.


DEPENDENCIES
------------
- domain
- domain_config
- domain_site_settings
