<?php

namespace Drupal\dsl\Routing;

// Classes referenced in this class.
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class DslRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Change form for the domain_site_settings.config_form route
    // to Drupal\dsl\Form\DslDomainConfigSettingsForm
    // First, we need to act only on the domain_site_settings.config_form route.
    if ($route = $collection->get('domain_site_settings.config_form')) {
      // Next, we need to set the value for _form to the form we have created.
      $route->setDefault('_form', 'Drupal\dsl\Form\DslDomainConfigSettingsForm');
    }
  }

}
