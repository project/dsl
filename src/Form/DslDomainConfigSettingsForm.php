<?php

namespace Drupal\dsl\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Routing\RequestContext;
use Drupal\file\Entity\File;
use Drupal\domain_site_settings\Form\DomainConfigSettingsForm;

/**
 * Extend Domain site settings configration.
 */
class DslDomainConfigSettingsForm extends DomainConfigSettingsForm {
  /**
   * The path alias manager.
   *
   * @var \Drupal\Core\Path\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The path validator.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * The request context.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected $requestContext;

  /**
   * Constructs a SiteInformationForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Path\AliasManagerInterface $alias_manager
   *   The path alias manager.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   The path validator.
   * @param \Drupal\Core\Routing\RequestContext $request_context
   *   The request context.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AliasManagerInterface $alias_manager, PathValidatorInterface $path_validator, RequestContext $request_context) {
    parent::__construct($config_factory, $alias_manager, $path_validator, $request_context);
    $this->aliasManager = $alias_manager;
    $this->pathValidator = $path_validator;
    $this->requestContext = $request_context;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'), $container->get('path.alias_manager'), $container->get('path.validator'), $container->get('router.request_context')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Retrieve the domain_site_settings configuration.
    $config = $this->config('domain_site_settings.domainconfigsettings');
    $site_config = $this->config('system.site');

    $domain_id = $this->getRequest()->get('domain_id');

    // Get the original form from the class we are extending.
    $form = parent::buildForm($form, $form_state);

    // Add a textarea to the site information section of the form for our
    // description.
    $form['site_information']['site_logo'] = [
      '#title' => t('Site logo'),
      '#type' => 'managed_file',
      '#default_value' => ($config->get($domain_id) != NULL) ? $config->get($domain_id . '.site_logo') : [$site_config->get('site_logo')],
      '#description' => t('allowed only gif png jpg jpeg file format'),
      '#upload_location' => 'public://logo/',
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
        'file_validate_size' => [25600000],
      ],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $domain_id = $this->getRequest()->get('domain_id');
    $logo = $form_state->getValue('site_logo');

    $config = $this->config('domain_site_settings.domainconfigsettings');

    if (!empty($logo)) {
      $file = File::load(reset($logo));
      $file->setPermanent();
      $file->save();

      $config->set($domain_id . '.site_logo', $logo);
      // Make sure to save the configuration.
      $config->save();

    }

    // Pass the remaining values off to the original form that we have extended,
    // so that they are also saved.
    parent::submitForm($form, $form_state);
  }

}
